package com.jcr.modelo;

import com.jcr.util.Util;

public class Suma extends Operacion{

	public Suma(Number primerOperando, Number segundoOperando) {
		super(primerOperando, segundoOperando);
	}

	/**
	 * @return	El resultado de realizar la suma entre {@code operando1} y {@code operando2}
	 */
	@Override
	public Number realizarOperacion() {
		Number resultado = this.operando1.doubleValue() + this.operando2.doubleValue();
		
		// Se trunca el numero, en caso que no tenga decimales
		return Util.tieneDecimales(resultado) ? resultado : resultado.longValue();
	}
	
	

}
