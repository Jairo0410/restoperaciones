package com.jcr.modelo;

/**
 * 
 * @author jairo
 * Objeto que simula una operacion aritmetica binaria (2 operandos)
 */
public abstract class Operacion {
	
	protected Number operando1;
	protected Number operando2;
	
	public Operacion(Number operando1, Number operando2) {
		this.operando1 = operando1;
		this.operando2 = operando2;
	}
	
	public Number getOperando1() {
		return operando1;
	}

	public void setOperando1(Number operando1) {
		this.operando1 = operando1;
	}

	public Number getOperando2() {
		return operando2;
	}

	public void setOperando2(Number operando2) {
		this.operando2 = operando2;
	}

	public abstract Number realizarOperacion() throws ArithmeticException;
}
