package com.jcr.modelo;

import com.jcr.util.Util;

public class Division extends Operacion {

	public Division(Number primerOperando, Number segundoOperando) {
		super(primerOperando, segundoOperando);
	}
	
	/**
	 * @return	El resultado de realizar la division entre {@code operando1} y {@code operando2}
	 */
	@Override
	public Number realizarOperacion() throws ArithmeticException {
		if(operando2.doubleValue() == 0) {
			throw new ArithmeticException("No se puede realizar division por cero");
		}
		
		Number resultado = this.operando1.doubleValue() / this.operando2.doubleValue();
		
		// Se trunca el numero, en caso que no tenga decimales
		return Util.tieneDecimales(resultado) ? resultado : resultado.longValue();
	}

}
