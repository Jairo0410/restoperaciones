package com.jcr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestOperacionesApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestOperacionesApplication.class, args);
	}

}
