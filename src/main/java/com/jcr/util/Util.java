package com.jcr.util;

public class Util {
	/**
	 * 
	 * @param numero	Numero que puede ser de punto flotante o entero
	 * @return			Verdadero si la parte decimal de {@code numero} es igual a 0, falso de otra forma
	 */
	public static boolean tieneDecimales(Number numero) {
		return numero.intValue() != numero.doubleValue();
	}
}
