package com.jcr.controlador;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jcr.modelo.Division;
import com.jcr.modelo.Multiplicacion;
import com.jcr.modelo.Resta;
import com.jcr.modelo.Suma;

@Controller
@RequestMapping("/operacion")
public class OperacionControlador {

	
	@PostMapping(path= "/suma")
	public ResponseEntity<Object> sumar(@RequestBody Suma suma) {
		Map<String, Object> respuesta = new HashMap<>();
		
		/**
		 * El siguiente if es verdadero en caso que a la hora de realizar 
		 * el "parsing" del cuerpo de la consulta, algun parametro sea insatisfecho
		 */
		if(suma == null || suma.getOperando1() == null || suma.getOperando2() == null) {
			respuesta.put("respuesta", "Se esperan 2 parametros numericos");
			
			return new ResponseEntity<>(respuesta, HttpStatus.BAD_REQUEST);
		}
		
		respuesta.put("respuesta", suma.realizarOperacion());
		
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}
	
	@PostMapping(path= "/resta")
	public ResponseEntity<Object> restar(@RequestBody Resta resta) {
		Map<String, Object> respuesta = new HashMap<>();
		
		if(resta == null || resta.getOperando1() == null || resta.getOperando2() == null) {
			respuesta.put("respuesta", "Se esperan 2 parametros numericos");
			
			return new ResponseEntity<>(respuesta, HttpStatus.BAD_REQUEST);
		}
		
		respuesta.put("respuesta", resta.realizarOperacion());
		
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}
	
	@PostMapping(path= "/multiplica")
	public ResponseEntity<Object> multiplicar(@RequestBody Multiplicacion multiplicacion) {
		Map<String, Object> respuesta = new HashMap<>();
		
		if(multiplicacion == null || multiplicacion.getOperando1() == null || multiplicacion.getOperando2() == null) {
			respuesta.put("respuesta", "Se esperan 2 parametros numericos");
			
			return new ResponseEntity<>(respuesta, HttpStatus.BAD_REQUEST);
		}
		
		respuesta.put("respuesta", multiplicacion.realizarOperacion());
		
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}
	
	@PostMapping(path= "/divide")
	public ResponseEntity<Object> dividir(@RequestBody Division division) {
		Map<String, Object> respuesta = new HashMap<>();
		
		if(division == null || division.getOperando1() == null || division.getOperando2() == null) {
			respuesta.put("respuesta", "Se esperan 2 parametros numericos");
			
			return new ResponseEntity<>(respuesta, HttpStatus.BAD_REQUEST);
		}
		
		/**
		 * La operacion de division es la unica que puede indefinirse, se maneja posible excepcion
		 * Ej: division entre cero
		 */
		try {
			respuesta.put("respuesta", division.realizarOperacion());
			
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		} catch(Exception ex) {
			respuesta.put("respuesta", ex.getMessage());
			
			return new ResponseEntity<>(respuesta, HttpStatus.BAD_REQUEST);
		}
	}
}
